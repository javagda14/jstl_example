<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 9/28/18
  Time: 8:59 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Index</title>
</head>
<body>
<c:out value="Hello world!"/>

<c:forEach var="i" begin="1" end="50">
    <c:out value="${i}"/>
</c:forEach>

</body>
</html>
