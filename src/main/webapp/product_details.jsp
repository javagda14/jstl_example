<%@ page import="com.jstl.Product" %>
<%@ page import="java.time.LocalDate" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 10/2/18
  Time: 6:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Product details</title>
</head>
<body>
<%--Wypisanie parametrów formularza z product form w tabelce.--%>
<%--Tutaj stwórz tabelkę--%>
<h2>Product details:</h2>
<%-- uzyć tagow JAVA do stworzenia obiektu createdProduct--%>
<%
    if (request.getParameter("name") != null &&
            request.getParameter("amount") != null &&
            request.getParameter("price") != null &&
            request.getParameter("expiredate") != null) {

        Product createdProduct = new Product();
        createdProduct.setName(request.getParameter("name"));
        createdProduct.setPrice(Double.parseDouble(request.getParameter("price")));
        createdProduct.setAmount(Integer.parseInt(request.getParameter("amount")));
        createdProduct.setExpireDate(LocalDate.parse(request.getParameter("expiredate")));

        pageContext.setAttribute("createdProduct", createdProduct);
    }
%>

<c:choose> <%--switch--%>
    <c:when test="${not empty createdProduct}"> <%--case--%>
        <%--Tabelka--%>
        <table>
            <tr>
                <td>Name:</td>
                <td>
                    <c:out value="${createdProduct.name}"/>
                </td>
            </tr>
        </table>
    </c:when>
    <c:otherwise> <%--default--%>
        <h3>Product details missing!</h3>
    </c:otherwise>
</c:choose>
</body>
</html>
