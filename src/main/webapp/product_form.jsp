<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 10/2/18
  Time: 6:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Product form</title>
</head>
<body>
<%--W formularzu powinny znaleźć się pola opisujące produkt--%>
<%--Tutaj stwórz formularz--%>
<h2>Formularz produktu</h2>
<form action="product_details.jsp" method="get">
    <div>
        <label for="name">Name:</label>
        <input type="text" name="name" id="name">
    </div>
    <div>
        <label for="price">Price:</label>
        <input type="number" name="price" id="price" step="0.01">
    </div>
    <div>
        <label for="expiredate">Expire date:</label>
        <input type="date" name="expiredate" id="expiredate">
    </div>
    <div>
        <label for="amount">Amount:</label>
        <input type="number" name="amount" id="amount">
    </div>
    <input type="submit" value="Send">
</form>
</body>
</html>
