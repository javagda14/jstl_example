<%@ page import="com.jstl.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.time.LocalDate" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 10/2/18
  Time: 7:23 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student Add</title>
</head>
<body>
<%! int licznik = 0; %>

<%
    List<Student> studentList;
    if (session.getAttribute("student_list") != null) {
        // wczytanie listy studentów z sesji
        studentList = (List<Student>) session.getAttribute("student_list");
    } else {
        // jeśli studentów nie ma to stworzenie nowej listy
        studentList = new ArrayList<>();
    }

    ///////////////////////////////////////////////////////////////////////////

    // 1. stworzenie studenta z parametrów (request.getParameter)...
    // odczytaj parametry i ustaw pola w instancji klasy student
    Student student = new Student();
    student.setId(licznik++);
    student.setIndex(request.getParameter("index"));
    student.setName(request.getParameter("name"));
    student.setSurname(request.getParameter("surname"));
    student.setBirthdate(LocalDate.parse(request.getParameter("birthdate")));

    // 2. dodanie studenta do listy
    studentList.add(student);

    // 3. zapisanie listy do sesji
    session.setAttribute("student_list", studentList);

    // 4. przekierowanie na stronę z listą studentów
    response.sendRedirect("/students/student_list.jsp");

    // 5. przejdź do listy studentów i rozpocznij implementację ich wyświetlania w jstl
    // NIE UŻYWAJ JAVY!
    ///////////////////////////////////////////////////////////////////////////

    //    response.sendRedirect("error.jsp");
%>
</body>
</html>
