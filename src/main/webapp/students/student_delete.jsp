<%@ page import="com.jstl.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 10/2/18
  Time: 8:51 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student Delete</title>
</head>
<body>
<%
    List<Student> studentList;
    if (session.getAttribute("student_list") != null) {
        // wczytanie listy studentów z sesji
        studentList = (List<Student>) session.getAttribute("student_list");
    } else {
        // jeśli studentów nie ma to stworzenie nowej listy
        studentList = new ArrayList<>();
    }

    String removedId = request.getParameter("id");
    int id = Integer.parseInt(removedId);

    // tworzę iterator listy students
    Iterator<Student> it = studentList.iterator();
    // dopóki mam następne elementy
    while (it.hasNext()) {
        // przechodzę do następnego elementu
        Student student = it.next();

        // jeśli obecny element to ten, który mam usunąć
        if (student.getId() == id) {
            // to usuwam element
            it.remove();
            // i kończę obieg pętli
            break;
        }
    }

    // umieszczam listę z powrotem w sesji
    session.setAttribute("student_list", studentList);

    response.sendRedirect("/students/student_list.jsp");
%>
</body>
</html>
