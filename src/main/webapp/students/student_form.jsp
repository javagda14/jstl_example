<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="com.jstl.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 10/2/18
  Time: 7:23 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student form</title>
</head>
<body>
<%@include file="/students/header.jsp" %>

<%
    List<Student> studentList;
    if (session.getAttribute("student_list") != null) {
        // wczytanie listy studentów z sesji
        studentList = (List<Student>) session.getAttribute("student_list");
    } else {
        // jeśli studentów nie ma to stworzenie nowej listy
        studentList = new ArrayList<>();
    }
    ///////////////////////////////////////////////////////////////////////
    if (request.getParameter("id") != null) {
        int id = Integer.parseInt(request.getParameter("id"));

        for (Student st : studentList) {
            if (st.getId() == id) {
                pageContext.setAttribute("student", st);
                break;
            }
        }
    }
%>

<h2>Formularz dodawania studenta:</h2>

<c:choose>
    <c:when test="${not empty student}">
        <form action="student_modify.jsp" method="get">
            <input type="hidden" name="id" value="<c:out value="${student.id}"/>">
            <div>
                <label for="name">Name: </label>
                <input id="name" name="name" type="text" value="<c:out value="${student.name}"/>">
            </div>
            <div>
                <label for="surname">Surname: </label>
                <input id="surname" name="surname" type="text" value="<c:out value="${student.surname}"/>">
            </div>
            <div>
                <label for="index">Index: </label>
                <input id="index" name="index" type="text" value="<c:out value="${student.index}"/>">
            </div>
            <div>
                <label for="birthdate">Birth date: </label>
                <input id="birthdate" name="birthdate" type="date" value="<c:out value="${student.birthdate}"/>">
            </div>
            <input type="submit" value="Zatwierdź">
        </form>
    </c:when>
    <c:otherwise>
        <form action="student_add.jsp" method="get">
            <div>
                <label for="name">Name: </label>
                <input id="name" name="name" type="text">
            </div>
            <div>
                <label for="surname">Surname: </label>
                <input id="surname" name="surname" type="text">
            </div>
            <div>
                <label for="index">Index: </label>
                <input id="index" name="index" type="text">
            </div>
            <div>
                <label for="birthdate">Birth date: </label>
                <input id="birthdate" name="birthdate" type="date">
            </div>
            <input type="submit" value="Dodaj">
        </form>
    </c:otherwise>
</c:choose>
</body>
</html>
