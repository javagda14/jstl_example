<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>--%>
<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 10/2/18
  Time: 7:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student List</title>
</head>
<body>

<%@include file="/students/header.jsp" %>

<h2>Lista Studentów</h2>

<table>
    <thead>
    <th>id</th>
    <th>name</th>
    <th>surname</th>
    <th>index</th>
    <th>birthdate</th>
    <th>actions</th>
    </thead>
    <tbody>
    <c:forEach var="student" items="${sessionScope.student_list}">
        <tr>
            <td>
                <c:out value="${student.id}"/>
            </td>
            <td>
                <c:out value="${student.name}"/>
            </td>
            <td>
                <c:out value="${student.surname}"/>
            </td>
            <td>
                <c:out value="${student.index}"/>
            </td>
            <td>
                <c:out value="${student.birthdate}"/>
            </td>
            <td>
                <table>
                    <tr>
                        <td><%--link usuwania--%>
                            <a href="student_delete.jsp?id=<c:out value="${student.id}"/>">Usuń</a>
                        </td>
                        <td><%--link modyfikacji--%>
                            <a href="student_form.jsp?id=<c:out value="${student.id}"/>">Edytuj</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
