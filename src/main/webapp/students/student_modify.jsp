<%@ page import="com.jstl.Student" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.time.LocalDate" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 10/3/18
  Time: 6:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student Modify</title>
</head>
<body>
<%
    List<Student> studentList;
    if (session.getAttribute("student_list") != null) {
        // wczytanie listy studentów z sesji
        studentList = (List<Student>) session.getAttribute("student_list");
    } else {
        // jeśli studentów nie ma to stworzenie nowej listy
        studentList = new ArrayList<>();
    }
    ///////////////////////////////////////////////////////////////////////

    int id = Integer.parseInt(request.getParameter("id"));

    Student searched = null;
    for (Student st : studentList) {
        if (st.getId() == id) {
            searched = st;
            break;
        }
    }

    searched.setName(request.getParameter("name"));
    searched.setSurname(request.getParameter("surname"));
    searched.setIndex(request.getParameter("index"));
    searched.setBirthdate(LocalDate.parse(request.getParameter("birthdate")));

    session.setAttribute("student_list", studentList);

    response.sendRedirect("/students/student_list.jsp");
%>
</body>
</html>
