<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 10/2/18
  Time: 5:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tabliczka static</title>
</head>
<body>

<h2>Tabliczka mnożenia static</h2>

<table>
    <c:forEach var="i" begin="1" end="50">
        <tr>
            <c:forEach var="j" begin="1" end="50">
                <td>
                    <c:out value="${i*j} "/>
                </td>
            </c:forEach>
        </tr>
    </c:forEach>
</table>
</body>
</html>
